﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeEquilibrio.Pojo
{
    class PE_U_table
    {
        public PE_U_table() { }

        public PE_U_table(int unidades_vendidas, double costo_Variable, double costo_fijo, double costo_Total, double ingreso_Total, double utilidad)
        {
            Unidades_vendidas = unidades_vendidas;
            Costo_Variable = costo_Variable;
            Costo_fijo = costo_fijo;
            Costo_Total = costo_Total;
            Ingreso_Total = ingreso_Total;
            Utilidad = utilidad;
        }

        public int Unidades_vendidas { get; set; }
        public double Costo_Unitario { get; set; }
        public double Costo_Variable { get; set; }
        public double Costo_fijo { get; set; }
        public double Costo_Total { get; set; }
        public double Ingreso_Total { get; set; }
        public double Utilidad { get; set; }

    }

}
