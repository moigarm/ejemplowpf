﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeEquilibrio.Pojo
{
    class PE_M_Table
    {

        public PE_M_Table(int unidades_vendidas, double costo_varable, double costo_fijo, double costo_total, double ingreso_total, double utilida)
        {
            this.unidades_vendidas = unidades_vendidas;
            this.costo_varable = costo_varable;
            this.costo_fijo = costo_fijo;
            this.costo_total = costo_total;
            this.ingreso_total = ingreso_total;
            this.utilida = utilida;
        }

        public int unidades_vendidas { get; set; }
        public double costo_unitario { get; set; }
        public double costo_varable { get; set; }
        public double costo_fijo { get; set; }
        public double costo_total { get; set; }
        public double ingreso_total { get; set; }
        public double utilida { get; set; } 
    }
}
