﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeEquilibrio.Pojo
{
    [Serializable]
    class Usuario
    {
        public Usuario() { }
        public Usuario(int id, string username, string password)
        {
            this.id = id;
            this.username = username;
            this.password = password;
        }

        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
