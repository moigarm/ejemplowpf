﻿using PuntoDeEquilibrio.Archivos;
using PuntoDeEquilibrio.PEMoney;
using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        tablePEMoney p = new tablePEMoney();
        ArrayList us = new ArrayList();
        UsuarioFile uf = new UsuarioFile();
        Boolean access = false;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            acceder();
        }

        private void acceder() // metodo para acceder al sistema
        {
            if (textBox.Text.Length > 0 && passwordBox.Password.ToString().Length > 0)// verifica que los campos no esten vacios
            {
                Usuario[] user = uf.findAll(); // llenamos un arreglo de Usuario con todos lo usuarios que existen en el archivo
                if (user != null)// verificamos que el arreglo no este vacio es decir que si existen usuarios en el archivo
                {

                    foreach (Usuario item in user)// recorremos el arreglo 
                    {
                        //verificamos si los datos en las cajas de texto coinciden con algun usuarion en el archivo
                        if ((item.username.Equals(textBox.Text, StringComparison.InvariantCultureIgnoreCase)
                            && item.password.Equals(passwordBox.Password.ToString())) || (textBox.Text.Equals("admin", StringComparison.InvariantCultureIgnoreCase)
                            && passwordBox.Password.ToString().Equals("admin")))
                        {
                            this.Hide();
                            View.Menu m = new View.Menu();
                            m.Show();
                            access = true;
                            break;
                        }
                    }
                }
                else
                {
                    // verificamos si las credenciales son las de administrador
                    if (textBox.Text.Equals("admin", StringComparison.InvariantCultureIgnoreCase) && passwordBox.Password.ToString().Equals("admin"))
                    {
                        this.Hide();
                        View.Menu m = new View.Menu();
                        m.Show();
                        access = true;

                    }

                }
                //verificamos si las credenciales no son correctas
                if (access == false)
                {
                    MessageBox.Show("Credenciales Incorrectas");
                }
            }
            else
            {
                MessageBox.Show("TODOS LOS CAMPOS SON REQUERIDOS", "ERROR");
            }

        }
    }
}
