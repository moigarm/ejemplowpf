﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)//evento del boton X
        {
            Environment.Exit(0);
        }


        /*El resto de eventos son para mostrar cada una de las ventanas del proyecto*/
        private void btnPE_M_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            PE_M pm = new PE_M();
            pm.ShowDialog();
        }

        private void btnPE_U_Click(object sender, RoutedEventArgs e)
        {
            // this.Close();
            PE_U pu = new PE_U();
            pu.ShowDialog();
        }

        private void btnUsurarios_Click(object sender, RoutedEventArgs e)
        {
            //this.Close();
            Menu_Usuarios mu = new Menu_Usuarios();
            mu.ShowDialog();
        }
    }
}
