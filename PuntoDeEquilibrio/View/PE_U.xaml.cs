﻿using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para PE_U.xaml
    /// </summary>
    public partial class PE_U : Window
    {
        List<PE_U_table> PEU = new List<PE_U_table>();


        public PE_U()
        {
            InitializeComponent();
            tablaPU.IsReadOnly = true;
        }
        
        private void btnCal_Click(object sender, RoutedEventArgs e)// este es el metodo del boton
        {
            label5.Content = "El punto de equilibrio Unitario es: " + PE();// se muestra el punto de equibrio en un label
            tablaPU.ItemsSource = loadTable(PE());// se llena la tabla con los datos del punto de equilibrio
            tablaPU.Columns[1].Visibility = Visibility.Collapsed; // se oculata la primer columna de la tabla
            this.btnCal.IsEnabled = false;
        }

        private float PE() // se calculo el punto de equilibrio por unidades y ese retorna un float
        {
           return Convert.ToSingle(txtCFT.Text) / (Convert.ToSingle(txtPVU.Text) - Convert.ToSingle(txtCVU.Text));
        }

        private List<PE_U_table> loadTable(float pe)// metodo para llenar una lista y se rotorna la misma
        {
          /* se agregan 5 elementos de tipo PE_U_table los cuales se usan par apoder identificar en que momento se alcanza el ounto de quilibrio*/
          PEU.Clear();
          PEU.Add(new PE_U_table(0,
                0 * Convert.ToSingle(txtCVU.Text),
                Convert.ToSingle(txtCFT.Text),
                (0 * Convert.ToSingle(txtCVU.Text) + Convert.ToSingle(txtCFT.Text)),
                (0 * Convert.ToSingle(txtPVU.Text)),
                (0 * Convert.ToSingle(txtPVU.Text)) - ((0 * Convert.ToSingle(txtCVU.Text)) + Convert.ToSingle(txtCFT.Text))));

            PEU.Add(new PE_U_table((Convert.ToInt32(pe / 2)),
                (Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtCVU.Text),
                Convert.ToSingle(txtCFT.Text),
                ((Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtCVU.Text) + Convert.ToSingle(txtCFT.Text)),
                ((Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtPVU.Text)),
                ((Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtPVU.Text)) - (((Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtCVU.Text)) + Convert.ToSingle(txtCFT.Text))));

            PEU.Add(new PE_U_table((Convert.ToInt32(pe)),
               (Convert.ToInt32(pe)) * Convert.ToSingle(txtCVU.Text),
               Convert.ToSingle(txtCFT.Text),
               ((Convert.ToInt32(pe)) * Convert.ToSingle(txtCVU.Text) + Convert.ToSingle(txtCFT.Text)),
               ((Convert.ToInt32(pe)) * Convert.ToSingle(txtPVU.Text)),
               ((Convert.ToInt32(pe)) * Convert.ToSingle(txtPVU.Text)) - (((Convert.ToInt32(pe) * Convert.ToSingle(txtCVU.Text)) + Convert.ToSingle(txtCFT.Text)))));

            PEU.Add(new PE_U_table((Convert.ToInt32(pe * 2)),
               (Convert.ToInt32(pe * 2)) * Convert.ToSingle(txtCVU.Text),
               Convert.ToSingle(txtCFT.Text),
               ((Convert.ToInt32(pe * 2)) * Convert.ToSingle(txtCVU.Text) + Convert.ToSingle(txtCFT.Text)),
               ((Convert.ToInt32(pe * 2)) * Convert.ToSingle(txtPVU.Text)),
               ((Convert.ToInt32(pe * 2)) * Convert.ToSingle(txtPVU.Text)) - (((Convert.ToInt32(pe / 2)) * Convert.ToSingle(txtCVU.Text)) + Convert.ToSingle(txtCFT.Text))));

            PEU.Add(new PE_U_table((Convert.ToInt32(pe * 4)),
               (Convert.ToInt32(pe * 4)) * Convert.ToSingle(txtCVU.Text),
               Convert.ToSingle(txtCFT.Text),
               ((Convert.ToInt32(pe * 4)) * Convert.ToSingle(txtCVU.Text) + Convert.ToSingle(txtCFT.Text)),
               ((Convert.ToInt32(pe * 4)) * Convert.ToSingle(txtPVU.Text)),
               ((Convert.ToInt32(pe * 4)) * Convert.ToSingle(txtPVU.Text)) - (((Convert.ToInt32(pe * 4)) * Convert.ToSingle(txtCVU.Text)) + Convert.ToSingle(txtCFT.Text))));


            /*-------------------------*/

            ((ScatterSeries)chart.Series[0]).ItemsSource = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>(Convert.ToString(0),Convert.ToInt32(((pe* Convert.ToSingle(txtCVU.Text)) +(Convert.ToSingle(txtCFT.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe/2),Convert.ToInt32(((pe* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe),Convert.ToInt32(((pe* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe*2),Convert.ToInt32(((pe* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe*4),Convert.ToInt32(pe))};
            ((ScatterSeries)chart.Series[1]).ItemsSource = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>(Convert.ToString(0),Convert.ToInt32(((0* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe/2),Convert.ToInt32((((pe/2)* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe),Convert.ToInt32(((pe* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe*2),Convert.ToInt32((((pe*2)* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text)))))),
                                    new KeyValuePair<string, int>(Convert.ToString(pe*4),Convert.ToInt32((((pe*4)* Convert.ToSingle(txtCVU.Text) +(Convert.ToSingle(txtCFT.Text))))))};

            return PEU;// se retorna la lista PEU
        }

    }
}
