﻿using PuntoDeEquilibrio.Archivos;
using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para NuevoUsuario.xaml
    /// </summary>
    public partial class NuevoUsuario : Window
    {
        UsuarioFile uf = new UsuarioFile();
        public NuevoUsuario()
        {
            InitializeComponent();
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            verificacion();

        }

        private void verificacion()//este metodo agrega un nuevo usuario
        {
            if (txtpass.Password.Length > 0 && txtUser.Text.Length > 0)//verificamos que los campos no esten vacios
            {
                Usuario user = new Usuario();//instacniomos un objeto de Usuario
                user.id = Convert.ToInt32(uf.countId() + 1);// agregamos el id 
                user.username = txtUser.Text;//agregam os el usuario
                user.password = txtpass.Password.ToString();//agregamos el password
                uf.Serializar(user);// serializamos el usuario y lo escribimos en el archivo
                MessageBox.Show("Usuario agregado con exito");// mostramos un mensaje de confirmacion
                this.Close();//cerramos la ventana
            }
            else
            {
                MessageBox.Show("Todos los campos son necesarios", "ERROR", MessageBoxButton.OK);
            }
        }
    }
}
