﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para Menu_Usuarios.xaml
    /// </summary>
    public partial class Menu_Usuarios : Window
    {
        public Menu_Usuarios()
        {
            InitializeComponent();
        }

        /*aqui cargamos todas las ventanas referentes a los usuarios*/
        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            NuevoUsuario nu = new NuevoUsuario();
            nu.ShowDialog();
        }

        private void btnUpdateUser_Click(object sender, RoutedEventArgs e)
        {
            ActualizarUsuario au = new ActualizarUsuario();
            au.ShowDialog();
        }
    }
}
