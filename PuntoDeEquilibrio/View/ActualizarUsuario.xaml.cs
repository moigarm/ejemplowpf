﻿using PuntoDeEquilibrio.Archivos;
using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para ActualizarUsuario.xaml
    /// </summary>
    public partial class ActualizarUsuario : Window
    {
        List<Usuario> u = new List<Usuario>();
        UsuarioFile uf = new UsuarioFile();
        Usuario usuario = new Usuario();
        public ActualizarUsuario()
        {
            InitializeComponent();
        }

        private void CargarTabla()
        {
            Usuario[] us = uf.findAll();
            if (us == null)
            {
                return;
            }
            foreach (Usuario user in us)
            {
                u.Add(user);
            }
            if (u.Count > 0)
            {
                tableUsuario.ItemsSource = u;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            btnUpdate.IsEnabled = false;
            txtPassword.IsEnabled = false;
            txtUser.IsEnabled = false;
            this.tableUsuario.IsReadOnly = true;
            CargarTabla();
        }

        private void tableUsuario_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            usuario = (Usuario)tableUsuario.SelectedItem;
            txtPassword.IsEnabled = true;
            txtUser.IsEnabled = true;
            btnUpdate.IsEnabled = true;
            txtUser.Text = usuario.username;
            txtPassword.Password = usuario.password;

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (txtPassword.Password.ToString().Length > 0 && txtUser.Text.Length > 0)
            {
                usuario.username = txtUser.Text;
                usuario.password = txtPassword.Password.ToString();
                uf.update(usuario);
                MessageBox.Show("Usuario actualizado con exito", "INFORMACION", MessageBoxButton.OK);
                this.Close();
            }
            else
            {
                MessageBox.Show("Todos los Campos Son Necesarios", "ERROR", MessageBoxButton.OK);
            }
        }
    }
}
