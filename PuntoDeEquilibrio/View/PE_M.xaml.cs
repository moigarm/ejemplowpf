﻿using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PuntoDeEquilibrio.View
{
    /// <summary>
    /// Lógica de interacción para PE_M.xaml
    /// </summary>
    public partial class PE_M : Window
    {
        List<PE_M_Table> PEM = new List<PE_M_Table>();

        public PE_M()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)// este metodo es el metodo del boton 
        {
            //convertir en porciento 
            float porMC = Convert.ToSingle(txtMC.Text) / 100;
            //sacar el costo variable unitario
            float cstVarUnit = Convert.ToSingle(txtPVU.Text) * porMC;
            // se calcula el Punto de equilibrio por moneda
            float peM = PEMoneda();
            // se calcula el punto de equilibrio por unidades
            float peU = PEUnidad();
            //se muestra el resultado del punto de equilibrio en un label
            label4.Content = "El punto de equilibrio en moneda es de: " + PEMoneda();
            // se carga la tabla
            tablaPM.ItemsSource = loadTable(cstVarUnit, peU, peM);
            // oculatamoso la primer columna de la tabla
            tablaPM.Columns[1].Visibility = Visibility.Collapsed;
            //inhabilitamos el boton
            this.btnOK.IsEnabled = false;
        }

        private float PEMoneda()//  este metodo calcula  el punto de equilibrio por moneda y retorna un float
        {
            float porMC = Convert.ToSingle(txtMC.Text) / 100;//convertimos el margen de contribucion a porcentaje
            float cstVarUnit = Convert.ToSingle(txtPVU.Text) * porMC;// calculamos el costo variable unitario mutiplicando precio variable unitario * margen de contribucion
            float dif = Convert.ToSingle(txtPVU.Text) - cstVarUnit; // calculamos la diferencia de precio de venta unitario - costo variable unitario
            float frac = dif / Convert.ToSingle(txtPVU.Text); // dividimos la diferencia / precio de venta unitario
            return Convert.ToSingle(txtCFU.Text) / (1 - frac); // retornamos el punto de equilibrio (float)

        }

        private float PEUnidad()// este metodo calcula el punto de equilibrio en unidades y retorna un float
        {
            float porMC = Convert.ToSingle(txtMC.Text) / 100; // convertimos el margen de contribucion a porcentaje
            float cstVarUnit = Convert.ToSingle(txtPVU.Text) * porMC;// multiplicamos el costo variable unitario * margen de contribucion
            return Convert.ToSingle(txtCFU.Text) / cstVarUnit;// retornamos el residuo del costo fijo unitario / costo variable unitario
        }

        private List<PE_M_Table> loadTable(float cstVar, float peU, float peM) // llenamos la lista y retornamos la misma
        {
            PEM.Clear(); // se limpia la lista
            /*aqui se agregan 5 elementos de timpo PE_M_Table a la lista y asi poder mostrar en una tabla en que momento se cumple el punto de equilibrio*/
            PEM.Add(new PE_M_Table(0,
                (0 * cstVar),
                Convert.ToSingle(txtCFU.Text),
                (0 * cstVar) + Convert.ToSingle(txtCFU.Text),
                (0 * Convert.ToSingle(txtPVU.Text)),
                (0 * Convert.ToSingle(txtPVU.Text)) - ((0 * cstVar) + Convert.ToSingle(txtCFU.Text))));

            PEM.Add(new PE_M_Table(Convert.ToInt32(peU / 2),
                ((peU / 2) * cstVar),
                Convert.ToSingle(txtCFU.Text),
                ((peU / 2) * cstVar) + Convert.ToSingle(txtCFU.Text),
                ((peU / 2) * Convert.ToSingle(txtPVU.Text)),
                ((peU / 2) * Convert.ToSingle(txtPVU.Text)) - (((peU / 2) * cstVar) + Convert.ToSingle(txtCFU.Text))));

            PEM.Add(new PE_M_Table(Convert.ToInt32(peU),
                ((peU) * cstVar),
                Convert.ToSingle(txtCFU.Text),
                ((peU) * cstVar) + Convert.ToSingle(txtCFU.Text),
                ((peU) * Convert.ToSingle(txtPVU.Text)),
                ((peU) * Convert.ToSingle(txtPVU.Text)) - (((peU) * cstVar) + Convert.ToSingle(txtCFU.Text))));

            PEM.Add(new PE_M_Table(Convert.ToInt32(peU * 2),
                ((peU * 2) * cstVar),
                Convert.ToSingle(txtCFU.Text),
                ((peU * 2) * cstVar) + Convert.ToSingle(txtCFU.Text),
                ((peU * 2) * Convert.ToSingle(txtPVU.Text)),
                ((peU * 2) * Convert.ToSingle(txtPVU.Text)) - (((peU * 2) * cstVar) + Convert.ToSingle(txtCFU.Text))));

            PEM.Add(new PE_M_Table(Convert.ToInt32(peU * 2),
                ((peU * 4) * cstVar),
                Convert.ToSingle(txtCFU.Text),
                ((peU * 4) * cstVar) + Convert.ToSingle(txtCFU.Text),
                ((peU * 4) * Convert.ToSingle(txtPVU.Text)),
                ((peU * 4) * Convert.ToSingle(txtPVU.Text)) - (((peU * 2) * cstVar) + Convert.ToSingle(txtCFU.Text))));

            /*------------SE CARGA LA TABLA---------------*/
            ((ScatterSeries)chart.Series[0]).ItemsSource = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>(Convert.ToString(0),Convert.ToInt32(((peU* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU/2),Convert.ToInt32(((peU* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU),Convert.ToInt32(((peU* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU*2),Convert.ToInt32(((peU* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU*4),Convert.ToInt32(peM))};
            ((ScatterSeries)chart.Series[1]).ItemsSource = new KeyValuePair<string, int>[] {
                                    new KeyValuePair<string, int>(Convert.ToString(0),Convert.ToInt32(((0* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU/2),Convert.ToInt32((((peU/2)* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU),Convert.ToInt32(((peU* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU*2),Convert.ToInt32((((peU*2)* cstVar) +(Convert.ToSingle(txtCFU.Text))))),
                                    new KeyValuePair<string, int>(Convert.ToString(peU*4),Convert.ToInt32((((peU*4)* cstVar) +(Convert.ToSingle(txtCFU.Text)))))};


            return PEM;//se retorna la lista
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.tablaPM.IsReadOnly = true;// hacemos que la tabla sea solo para lectura
        }
    }
}
