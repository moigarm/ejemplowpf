﻿using PuntoDeEquilibrio.Pojo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PuntoDeEquilibrio.Archivos
{
    [Serializable]
    class UsuarioFile
    {
        /*esta clase es para crear el RamdomAccessFIle de usudaio y se serializan los datos para un mejor manejo de ellos*/
        Usuario[] usuarios;

        public void Serializar(Usuario p) // metod para seralizar un usuario
        {
            FileStream fs = new FileStream("dbUsuario.dat", FileMode.OpenOrCreate);
            BinaryFormatter bformatter = new BinaryFormatter();
            UsuarioFile dbp = new UsuarioFile();
            if (fs.Length != 0)
            {
                dbp = (UsuarioFile)bformatter.Deserialize(fs);
                Usuario[] arr = new Usuario[dbp.usuarios.Length + 1];
                dbp.usuarios.CopyTo(arr, 0);
                int idNewUsuario = dbp.usuarios.Length + 1;
                p.id = idNewUsuario;
                arr[idNewUsuario - 1] = p;
                fs.Position = 0;
                dbp.usuarios = arr;
                bformatter.Serialize(fs, dbp);
            }
            else
            {
                p.id = 1;
                dbp.usuarios = new Usuario[1];
                dbp.usuarios[0] = p;
                bformatter.Serialize(fs, dbp);
            }
            fs.Close();
        }
        public Usuario Deserializar(int id)// metodo para deserializar un usuairo por id
        {
            Usuario p = new Usuario();
            FileStream fs = new FileStream("dbUsuario.dat", FileMode.OpenOrCreate);
            BinaryFormatter bformatter = new BinaryFormatter();
            if (id == 0)
            {
                return p;
            }
            if (fs.Length != 0)
            {
                UsuarioFile dbp = (UsuarioFile)bformatter.Deserialize(fs);
                if (id <= dbp.usuarios.Length)
                {
                    p = dbp.usuarios[id - 1];
                }
            }
            fs.Close();
            return p;
        }

        public Usuario[] findAll() // metodo para buscar todos los usuarios
        {


            try
            {
                FileStream fs = new FileStream("dbUsuario.dat", FileMode.OpenOrCreate);
                BinaryFormatter bformatter = new BinaryFormatter();
                Usuario[] per = ((UsuarioFile)bformatter.Deserialize(fs)).usuarios;
                fs.Close();
                return per;
            }
            catch (SerializationException ex)
            {
                Console.WriteLine("", ex.ToString());
                return null;
            }

        }

        public int countId()//metodo para contar cuantos usuarios hay
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream("dbUsuario.dat", FileMode.OpenOrCreate);
                BinaryFormatter bformatter = new BinaryFormatter();
                Usuario[] per = ((UsuarioFile)bformatter.Deserialize(fs)).usuarios;
                fs.Close();
                return per.Length;
            }
            catch (SerializationException ex)
            {
                Console.WriteLine("", ex.ToString());
                fs.Close();
                return 0;
            }
        }

        public void update(Usuario s)// metodo para actualizar un usuario
        {
            FileStream fs = new FileStream("dbUsuario.dat", FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            UsuarioFile dbs = new UsuarioFile();
            int id = s.id;
            if (fs.Length != 0)
            {
                dbs = (UsuarioFile)bformatter.Deserialize(fs);
                Usuario[] arr = new Usuario[dbs.usuarios.Length];
                dbs.usuarios.CopyTo(arr, 0);
                arr[id - 1] = s;
                fs.Position = 0;
                dbs.usuarios = arr;
                bformatter.Serialize(fs, dbs);
                fs.Close();
            }
        }

        public void delete(Usuario s)// metodo para eliminar un usuario
        {
            FileStream fs = new FileStream("DdbUsuario.dat", FileMode.OpenOrCreate);
            BinaryFormatter bformatter = new BinaryFormatter();
            UsuarioFile dbs = new UsuarioFile();
            int id = s.id;
            if (fs.Length != 0)
            {
                dbs = (UsuarioFile)bformatter.Deserialize(fs);
                Usuario[] arr = new Usuario[dbs.usuarios.Length];

                dbs.usuarios.CopyTo(arr, 0);
                arr[id - 1] = null;
                fs.Position = 0;
                dbs.usuarios = arr;
                bformatter.Serialize(fs, dbs);
                fs.Close();
            }
        }
    }
}
