﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PuntoDeEquilibrio.PEUnits
{
    class tablePEUNIT
    {//creamos en objero tablePEUNIT con todos los atributos que usaremos para calcular el punto de equilibrio por unidades
        public tablePEUNIT() { }

        public tablePEUNIT(double unidades_Vendidas, double costo_Unitario, double costo_Variable, double costo_Fijo, double costo_Total, double ingreso_Total, double utilidad)
        {
            Unidades_Vendidas = unidades_Vendidas;
            Costo_Unitario = costo_Unitario;
            Costo_Variable = costo_Variable;
            Costo_Fijo = costo_Fijo;
            Costo_Total = costo_Total;
            Ingreso_Total = ingreso_Total;
            Utilidad = utilidad;
        }

        public double Unidades_Vendidas { get; set; }
        public double Costo_Unitario { get; set; }
        public double Costo_Variable { get; set; }
        public double Costo_Fijo { get; set; }
        public double Costo_Total { get; set; }
        public double Ingreso_Total { get; set; }
        public double Utilidad { get; set; }

    }
}
